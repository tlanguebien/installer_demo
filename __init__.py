"""
    add project to PYTHONPATH
"""

# pylint: disable=C0103
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '.')))
