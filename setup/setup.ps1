# Installation
$app_name = "MyApp"
Write-Output "Installing $app_name"

# Set Defaut installation folder
$default_installation_folder = "$Env:Programfiles/"

# Choose directory form
Write-Output "Please select an installation folder"
[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null
$folderselect = New-Object System.Windows.Forms.FolderBrowserDialog
$folderselect.Description = "$app_name - Select installation folder"
$folderselect.rootfolder = "MyComputer"
$folderselect.SelectedPath = $default_installation_folder
if($folderselect.ShowDialog() -ne "OK") {
    Write-Output "Installation cancelled by user"
    exit
} else {
    $selected_installation_folder = $folderselect.SelectedPath
    $temp_zip_file = "$selected_installation_folder/$app_name.zip"
    $unzip_folder = "$selected_installation_folder/$app_name unzip"
    $full_installation_folder = Join-Path -Path $selected_installation_folder -ChildPath $app_name

    # Download
    Write-Output "Downloading..."
    $WebClient = New-Object System.Net.WebClient
    $WebClient.DownloadFile("https://gitlab.com/tlanguebien/installer_demo/-/archive/master/installer_demo-master.zip",$temp_zip_file)

    # Unzip and rename
    Write-Output "Unzipping archive..."
    Expand-Archive -Path $temp_zip_file -DestinationPath $unzip_folder -Force
    $folder_to_move = resolve-path(Get-ChildItem $unzip_folder | % {$_.FullName})
    Move-Item $folder_to_move "$full_installation_folder" -Force

    # Remove useless files
    Write-Output "Cleaning..."
    Remove-Item $unzip_folder -Force
    Remove-Item $temp_zip_file

    # Create Shortcuts
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut("$full_installation_folder\$app_name.lnk")
    $Shortcut.TargetPath = "$full_installation_folder/Python37/pythonw.exe"
    $Shortcut.Arguments =  "`"$full_installation_folder/open_app.py`""
    $Shortcut.IconLocation = "$full_installation_folder/app/ui/icons/$app_name.ico"
    $Shortcut.Save()

    $DesktopPath = (join-path $env:USERPROFILE "desktop").Replace("\", "/")
    $Shortcut = $WshShell.CreateShortcut("$DesktopPath\$app_name.lnk")
    $Shortcut.TargetPath = "$full_installation_folder/Python37/pythonw.exe"
    $Shortcut.Arguments =  "`"$full_installation_folder/open_app.py`""
    $Shortcut.IconLocation = "$full_installation_folder/app/ui/icons/$app_name.ico"
    $Shortcut.Save()

    $Shortcut = $WshShell.CreateShortcut("$full_installation_folder\$app_name (debug mode).lnk")
    $Shortcut.TargetPath = "$full_installation_folder/Python37/python.exe"
    $Shortcut.Arguments =  "`"$full_installation_folder/open_app.py`""
    $Shortcut.IconLocation = "$full_installation_folder/app/ui/icons/$app_name.ico"
    $Shortcut.Save()

    if (-not $error) {
        Write-Host "The installation completed successfully. Thank you for downloading $app_name" -ForegroundColor "green"
        Start-Sleep 2
    } else {
        Write-Host "The installation encountered errors." -ForegroundColor "red"
        pause
    }
    
}

