#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

file_path = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0 , file_path)
os.chdir(file_path) # just in case the app is lauched from the wrong working directory

from app import MyApp

if __name__ == "__main__":
    MyApp.main()

