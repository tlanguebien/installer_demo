#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import os
import ctypes
import webbrowser

# import packages
from app.ui.qt_core import *
from app.ui.uis.windows.main_window.functions_main_window import *
from app.ui.core.json_settings import Settings
from app.ui.uis.windows.main_window.setup_main_window import SetupMainWindow
from app.ui.uis.windows.main_window.ui_main import UI_MainWindow
from app.ui.widgets import *
from app.UserPreferences import UserPreferences


class MainWindow(QMainWindow):
    """
    This class contains the main window
    """
    def __init__(self):
        """
        This function configures the main window
        """
        
        super().__init__()
        self.app = "MyApp"
        self.app_dir = os.getcwd()

        # Load Settings
        settings = Settings()
        self.settings = settings.items

        # Load widgets from "Core\uis\main_window\ui_main.py"
        self.ui = UI_MainWindow(self)
        SetupMainWindow.setup_gui(self)


    def btn_clicked(self):
        """
        This function is executed anytime a button (btn class) is clicked
        """
        # Get btn clicked
        btn = SetupMainWindow.setup_btns(self)
        print("{} button clicked".format(btn.objectName()))

        # Btn Settings
        if btn.objectName() == "btn_settings" or btn.objectName() == "btn_close_left_column":
            # Show / Hide
            MainFunctions.toggle_left_column(self)
            self.ui.left_menu.select_only_one_tab(btn.objectName())

        if btn.objectName() == "btn_documentation":
            # Open documentation
            webbrowser.open("https://www.youtube.com/watch?v=dQw4w9WgXcQ")


    def btn_released(self):
        """
        This function is executed whenever a button is released
        """
        # Get bt clicked
        btn = SetupMainWindow.setup_btns(self)
        print("{} button released".format(btn.objectName()))


    def resizeEvent(self, event):
        """
        This function is executed when the main window is resized
        """
        SetupMainWindow.resize_grips(self)


    def mousePressEvent(self, event):
        """
        This function is executed when the main window is clicked
        """
        # Set drag pos window
        self.dragPos = event.globalPos()


def main():
    """
    This function launches the application
    """
    # Get user's preferences
    user_preferences = UserPreferences()

    # Force Taskbar icon update
    icon_path = os.path.join(os.getcwd(), "app/ui/icons")
    icon_file = os.path.join(icon_path, "MyApp.ico")
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID('null')

    # Run Application
    App = QApplication(sys.argv)
    App.setWindowIcon(QIcon(icon_file))
    window = MainWindow()
    window.show()
    sys.exit(App.exec())
