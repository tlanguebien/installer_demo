#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import pathlib
import getpass
from app.scripts import Logs
from app.scripts import JsonUtils

SCRIPT = os.path.basename(__file__)

class UserPreferences():
    def __init__(self):
        self.preferences_path = self.get_preferences_path()
        self.userpreferences_file = self.get_user_preferences()
        
        # Get preferences, or fill them with default values
        self.user = self.get_user()
        self.log_as = self.get_log_as()
        self.debug_mode = self.get_debug_mode()

        # Result
        self.userpreferences = JsonUtils.read_json(self.userpreferences_file)

    
    def get_preferences_path(self):
        """
        Gets the preferences folder, and creates it if it doesnt exist
        returns the path (string) if succeeded, None otherwise
        """
        preferences_path = os.path.join(pathlib.Path.home(), "MyApp")
        try:
            if not os.path.isdir(preferences_path):
                os.mkdir(preferences_path)
                Logs.add_log("Creating preferences folder in {}".format(preferences_path),
                                                                        SCRIPT)
        except:
            Logs.add_log("Cound not create folder {}".format(preferences_path),
                                                             SCRIPT)
            return None
        return(preferences_path)


    def get_user_preferences(self):
        """
        Returns the user's preference json file
        Returns None if the file cannot be created
        """

        if not self.preferences_path :
            return None
        
        userpreferences = "user.json"
        userpreferences = os.path.join(self.preferences_path, userpreferences)
        if not JsonUtils.check_json_exist(userpreferences):
            try:
                JsonUtils.write_json({},userpreferences)
                Logs.add_log("Creating preference file {}".format(userpreferences), SCRIPT)
            except:
                Logs.add_log("Failed to create {}".format(userpreferences), SCRIPT)
                return None

        return userpreferences

        
    def get_user(self):
        """
        sets the user entry to the ldap's user CN
        """

        key = "user"
        user = getpass.getuser()
        JsonUtils.write_value(key, user, self.userpreferences_file)
        return user


    def get_log_as(self):
        """
        sets the field "log_as" to another user
        This function is very basic for now, we'll add more features when the log-as module will 
        be operational
        """

        key = "log_as"
        JsonUtils.write_value(key, None, self.userpreferences_file)
        return None
        

    def get_debug_mode(self):
        """
        Set debug mode to false by default
        """

        key = "debug_mode"
        data = JsonUtils.read_json(self.userpreferences_file)
        if not data.get(key):
            debugmode = False
            JsonUtils.write_value(key, debugmode, self.userpreferences_file)
            return debugmode

        debugmode = data[key]
        return debugmode




