#!/usr/bin/python
# -*- coding: utf-8 -*-

from app.ui.qt_core import *

class PyCircularProgress(QWidget):
    def __init__(
        self,
        value = 0,
        progress_width = 10,
        is_rounded = True,
        max_value = 100,
        progress_color = "#ff79c6",
        enable_text = True,
        font_family = "Segoe UI",
        font_size = 12,
        suffix = "%",
        text_color = "#ff79c6",
        enable_bg = True,
        bg_color = "#44475a"
    ):
        QWidget.__init__(self)

        # Custom properties
        self.value = value
        self.progress_width = progress_width
        self.progress_rounded_cap = is_rounded
        self.max_value = max_value
        self.progress_color = progress_color
        # Text
        self.enable_text = enable_text
        self.font_family = font_family
        self.font_size = font_size
        self.suffix = suffix
        self.text_color = text_color
        # BG
        self.enable_bg = enable_bg
        self.bg_color = bg_color

    # Add dropshadow
    def add_shadow(self, enable):
        if enable:
            self.shadow = QGraphicsDropShadowEffect(self)
            self.shadow.setBlurRadius(15)
            self.shadow.setXOffset(0)
            self.shadow.setYOffset(0)
            self.shadow.setColor(QColor(0, 0, 0, 80))
            self.setGraphicsEffect(self.shadow)

    # Set value
    def set_value(self, value):
        self.value = value
        self.repaint() # Render progress bar after change value


    # Paint event (design your circular progress here)
    def paintEvent(self, e):
        # SET PROGRESS PARAMETERS
        width = self.width() - self.progress_width
        height = self.height() - self.progress_width
        margin = self.progress_width / 2
        value =  self.value * 360 / self.max_value

        # Painter
        paint = QPainter()
        paint.begin(self)
        paint.setRenderHint(QPainter.Antialiasing) # remove pixelated edges
        paint.setFont(QFont(self.font_family, self.font_size))

        # Create rectangle
        rect = QRect(0, 0, self.width(), self.height())
        paint.setPen(Qt.NoPen)

        # Pen
        pen = QPen()             
        pen.setWidth(self.progress_width)
        # Set Round Cap
        if self.progress_rounded_cap:
            pen.setCapStyle(Qt.RoundCap)

        # Enable bg
        if self.enable_bg:
            pen.setColor(QColor(self.bg_color))
            paint.setPen(pen)  
            paint.drawArc(margin, margin, width, height, 0, 360 * 16) 

        # Create arc / circular progress
        pen.setColor(QColor(self.progress_color))
        paint.setPen(pen)      
        paint.drawArc(margin, margin, width, height, -90 * 16, -value * 16)       

        # Create text
        if self.enable_text:
            pen.setColor(QColor(self.text_color))
            paint.setPen(pen)
            paint.drawText(rect, Qt.AlignCenter, f"{self.value}{self.suffix}")

        # End
        paint.end()