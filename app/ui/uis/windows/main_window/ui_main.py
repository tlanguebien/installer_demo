#!/usr/bin/python
# -*- coding: utf-8 -*-

from app.ui.core.functions import Functions
from app.ui.qt_core import *
from app.ui.core.json_settings import Settings
from app.ui.core.json_themes import Themes
from app.ui.widgets import *
from . setup_main_window import *
from app.ui.uis.pages.ui_main_pages import Ui_MainPages
from app.ui.uis.columns.ui_right_column import Ui_RightColumn
from app.ui.widgets.py_credits_bar.py_credits import PyCredits
from app.UserPreferences import UserPreferences
import ctypes

# Adjust scale factor based on the size of the screen
os.environ["QT_FONT_DPI"] = "96"
user32 = ctypes.windll.user32
screenwidth = user32.GetSystemMetrics(79)
os.environ["QT_SCALE_FACTOR"] = "1"

class UI_MainWindow(object):
    def __init__(self, main_window) :
        super().__init__()
        self.app = main_window.app
        self.setup_ui(main_window)
        self.load_user_preferences(main_window)
        main_window.hide_grips = True # Show/Hide resize grips


    def load_user_preferences(self, main_window):
        """
        This function loads the user's preferences
        """
        main_window.userpreferences = UserPreferences()
        main_window.userpreferences = main_window.userpreferences.userpreferences


    def setup_ui(self, main_window):
        if not main_window.objectName():
            main_window.setObjectName("MainWindow")

        # Load settings
        settings = Settings()
        self.settings = settings.items

        # Load theme color
        themes = Themes()
        self.themes = themes.items

        # Set initial parameters
        main_window.resize(self.settings["startup_size"][0], self.settings["startup_size"][1])
        main_window.setMinimumSize(self.settings["minimum_size"][0], self.settings["minimum_size"][1])

        # Add central widget to app
        self.central_widget = QWidget()
        self.central_widget.setStyleSheet(f'''
            font: {self.settings["font"]["text_size"]}pt "{self.settings["font"]["family"]}";
            color: {self.themes["app_color"]["text_foreground"]};
        ''')
        self.central_widget_layout = QVBoxLayout(self.central_widget)
        if self.settings["custom_title_bar"]:
            self.central_widget_layout.setContentsMargins(10,10,10,10)
        else:
            self.central_widget_layout.setContentsMargins(0,0,0,0)
        
        # Add inside PyWindow "layout" all Widgets
        self.window = PyWindow(
            main_window,
            bg_color = self.themes["app_color"]["bg_one"],
            border_color = self.themes["app_color"]["bg_two"],
            text_color = self.themes["app_color"]["text_foreground"]
        )
        
        # If disable custom title bar
        if not self.settings["custom_title_bar"]:
            self.window.set_stylesheet(border_radius = 0, border_size = 0)
        
        # Add py window to central widget
        self.central_widget_layout.addWidget(self.window)

        # Add here the custom left menu bar
        left_menu_margin = self.settings["left_menu_content_margins"]
        left_menu_minimum = self.settings["left_menu_size"]["minimum"]
        self.left_menu_frame = QFrame()
        self.left_menu_frame.setMaximumSize(left_menu_minimum + (left_menu_margin * 2), 17280)
        self.left_menu_frame.setMinimumSize(left_menu_minimum + (left_menu_margin * 2), 0)

        # Left menu layout
        self.left_menu_layout = QHBoxLayout(self.left_menu_frame)
        self.left_menu_layout.setContentsMargins(
            left_menu_margin,
            left_menu_margin,
            left_menu_margin,
            left_menu_margin
        )

        # Add custom left menu here
        self.left_menu = PyLeftMenu(
            parent = self.left_menu_frame,
            app_parent = self.central_widget, # For tooltip parent
            dark_one = self.themes["app_color"]["dark_one"],
            dark_three = self.themes["app_color"]["dark_three"],
            dark_four = self.themes["app_color"]["dark_four"],
            bg_one = self.themes["app_color"]["bg_one"],
            icon_color = self.themes["app_color"]["icon_color"],
            icon_color_hover = self.themes["app_color"]["icon_hover"],
            icon_color_pressed = self.themes["app_color"]["icon_pressed"],
            icon_color_active = self.themes["app_color"]["icon_active"],
            context_color = self.themes["app_color"]["context_color"],
            text_foreground = self.themes["app_color"]["text_foreground"],
            text_active = self.themes["app_color"]["text_active"]
        )
        self.left_menu_layout.addWidget(self.left_menu)

        # Add here the left column with Stacked Widgets
        self.left_column_frame = QFrame()
        self.left_column_frame.setMaximumWidth(self.settings["left_column_size"]["minimum"])
        self.left_column_frame.setMinimumWidth(self.settings["left_column_size"]["minimum"])
        self.left_column_frame.setStyleSheet(f"background: {self.themes['app_color']['bg_two']}")

        # Add layout to left column
        self.left_column_layout = QVBoxLayout(self.left_column_frame)
        self.left_column_layout.setContentsMargins(0,0,0,0)

        # Add custom left menu widget
        self.left_column = PyLeftColumn(
            main_window,
            app_parent = self.central_widget,
            text_title = "Settings Left Frame",
            text_title_size = self.settings["font"]["title_size"],
            text_title_color = self.themes['app_color']['text_foreground'],
            icon_path = Functions.set_svg_icon("icon_settings.svg"),
            dark_one = self.themes['app_color']['dark_one'],
            bg_color = self.themes['app_color']['bg_three'],
            btn_color = self.themes['app_color']['bg_three'],
            btn_color_hover = self.themes['app_color']['bg_two'],
            btn_color_pressed = self.themes['app_color']['bg_one'],
            icon_color = self.themes['app_color']['icon_color'],
            icon_color_hover = self.themes['app_color']['icon_hover'],
            context_color = self.themes['app_color']['context_color'],
            icon_color_pressed = self.themes['app_color']['icon_pressed'],
            icon_close_path = Functions.set_svg_icon("icon_close.svg")
        )
        self.left_column_layout.addWidget(self.left_column)

        # Add here the right widgets
        self.right_app_frame = QFrame()

        # Add right app layout
        self.right_app_layout = QVBoxLayout(self.right_app_frame)
        self.right_app_layout.setContentsMargins(3,3,3,3)
        self.right_app_layout.setSpacing(6)

        # Add title bar frame
        self.title_bar_frame = QFrame()
        self.title_bar_frame.setMinimumHeight(40)
        self.title_bar_frame.setMaximumHeight(40)
        self.title_bar_layout = QVBoxLayout(self.title_bar_frame)
        self.title_bar_layout.setContentsMargins(0,0,0,0)
        
        # Add custom title bar to layout
        self.title_bar = PyTitleBar(
            main_window,
            logo_width = 100,
            app_parent = self.central_widget,
            logo_image = "atools_logotext_small_v001.png",
            bg_color = self.themes["app_color"]["bg_two"],
            div_color = self.themes["app_color"]["bg_three"],
            btn_bg_color = self.themes["app_color"]["bg_two"],
            btn_bg_color_hover = self.themes["app_color"]["bg_three"],
            btn_bg_color_pressed = self.themes["app_color"]["bg_one"],
            icon_color = self.themes["app_color"]["icon_color"],
            icon_color_hover = self.themes["app_color"]["icon_hover"],
            icon_color_pressed = self.themes["app_color"]["icon_pressed"],
            icon_color_active = self.themes["app_color"]["icon_active"],
            context_color = self.themes["app_color"]["context_color"],
            dark_one = self.themes["app_color"]["dark_one"],
            text_foreground = self.themes["app_color"]["text_foreground"],
            radius = 8,
            font_family = self.settings["font"]["family"],
            title_size = self.settings["font"]["title_size"],
            is_custom_title_bar = self.settings["custom_title_bar"]
        )
        self.title_bar_layout.addWidget(self.title_bar)

        # Add content area
        self.content_area_frame = QFrame()

        # Create layout
        self.content_area_layout = QHBoxLayout(self.content_area_frame)
        self.content_area_layout.setContentsMargins(0,0,0,0)
        self.content_area_layout.setSpacing(0)

        # Left content
        self.content_area_left_frame = QFrame()

        # Import main pages to content area
        self.load_pages = Ui_MainPages()
        self.load_pages.setupUi(self.content_area_left_frame)

        # Right bar
        self.right_column_frame = QFrame()
        self.right_column_frame.setMinimumWidth(self.settings["right_column_size"]["minimum"])
        self.right_column_frame.setMaximumWidth(self.settings["right_column_size"]["minimum"])

        # Import right column
        self.content_area_right_layout = QVBoxLayout(self.right_column_frame)
        self.content_area_right_layout.setContentsMargins(5,5,5,5)
        self.content_area_right_layout.setSpacing(0)

        # Right bg
        self.content_area_right_bg_frame = QFrame()
        self.content_area_right_bg_frame.setObjectName("content_area_right_bg_frame")
        self.content_area_right_bg_frame.setStyleSheet(f'''
        #content_area_right_bg_frame {{
            border-radius: 8px;
            background-color: {self.themes["app_color"]["bg_two"]};
        }}
        ''')

        # Add bg
        self.content_area_right_layout.addWidget(self.content_area_right_bg_frame)

        # Add right pages to right column
        self.right_column = Ui_RightColumn()
        self.right_column.setupUi(self.content_area_right_bg_frame)

        # Add to layouts
        self.content_area_layout.addWidget(self.content_area_left_frame)
        self.content_area_layout.addWidget(self.right_column_frame)

        # Credits / bottom app frame
        self.credits_frame = QFrame()
        self.credits_frame.setMinimumHeight(26)
        self.credits_frame.setMaximumHeight(26)

        # Create layout
        self.credits_layout = QVBoxLayout(self.credits_frame)
        self.credits_layout.setContentsMargins(0,0,0,0)

        # Add custom widget credits
        self.credits = PyCredits(
            bg_two = self.themes["app_color"]["bg_two"],
            app_name = self.settings["app_name"],
            version = self.settings["version"],
            credits = self.settings["credits"],
            font_family = self.settings["font"]["family"],
            text_size = self.settings["font"]["text_size"],
            text_description_color = self.themes["app_color"]["text_description"]
        )

        #  Add to layout
        self.credits_layout.addWidget(self.credits)

        # Add widgets to right layout
        self.right_app_layout.addWidget(self.title_bar_frame)
        self.right_app_layout.addWidget(self.content_area_frame)
        self.right_app_layout.addWidget(self.credits_frame)

        # Add here your custom widgets or default widgets
        self.window.layout.addWidget(self.left_menu_frame)
        self.window.layout.addWidget(self.left_column_frame)
        self.window.layout.addWidget(self.right_app_frame)

        # Add central widget and set content margins
        main_window.setCentralWidget(self.central_widget)

