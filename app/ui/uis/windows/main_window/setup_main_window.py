#!/usr/bin/python
# -*- coding: utf-8 -*-


from app.ui.widgets.py_table_widget.py_table_widget import PyTableWidget
from . functions_main_window import *
import getpass
import os
from app.ui.qt_core import *
from app.ui.core.json_settings import Settings
from app.ui.core.json_themes import Themes
from app.ui.widgets import *
from . ui_main import *
from . functions_main_window import *
from app.scripts import JsonUtils


class SetupMainWindow:
    def setup_btns(main_window):
        """
        Setup custom btns of custom widgets
        Get sender() function when btn is clicked
        """
        if main_window.ui.title_bar.sender() != None:
            return main_window.ui.title_bar.sender()
        elif main_window.ui.left_menu.sender() != None:
            return main_window.ui.left_menu.sender()
        elif main_window.ui.left_column.sender() != None:
            return main_window.ui.left_column.sender()


    def setup_gui(main_window):
        """
        Setup main window
        """
        # Load menus
        app_dir = main_window.app_dir
        menus_json = os.path.join(app_dir, "app", "menus.json")
        menus = JsonUtils.read_json(menus_json)
        if "left" in menus:
            add_left_menus = menus["left"]
        else:
            add_left_menus = []
        if "title_bar" in menus:
            add_title_bar_menus = menus["title_bar"]
        else:
            add_title_bar_menus = []


        add_title_bar_menus = []
        
        # app name
        main_window.setWindowTitle(main_window.settings["app_name"])
        if main_window.settings["custom_title_bar"]:
            main_window.setWindowFlag(Qt.FramelessWindowHint)
            main_window.setAttribute(Qt.WA_TranslucentBackground)

        # Grips
        if main_window.settings["custom_title_bar"]:
            main_window.left_grip = PyGrips(main_window, "left", main_window.hide_grips)
            main_window.right_grip = PyGrips(main_window, "right", main_window.hide_grips)
            main_window.top_grip = PyGrips(main_window, "top", main_window.hide_grips)
            main_window.bottom_grip = PyGrips(main_window, "bottom", main_window.hide_grips)
            main_window.top_left_grip = PyGrips(main_window, "top_left", main_window.hide_grips)
            main_window.top_right_grip = PyGrips(main_window, "top_right", main_window.hide_grips)
            main_window.bottom_left_grip = PyGrips(main_window, "bottom_left", main_window.hide_grips)
            main_window.bottom_right_grip = PyGrips(main_window, "bottom_right", main_window.hide_grips)

        # Left Menu
        main_window.ui.left_menu.add_menus(add_left_menus)
        main_window.ui.left_menu.clicked.connect(main_window.btn_clicked)
        main_window.ui.left_menu.released.connect(main_window.btn_released)

        # Add title bar buttons
        main_window.ui.title_bar.add_menus(add_title_bar_menus)

        # Set signals
        main_window.ui.title_bar.clicked.connect(main_window.btn_clicked)
        main_window.ui.title_bar.released.connect(main_window.btn_released)

        # Add title bar text
        main_window.ui.title_bar.set_title("You are logged as {}".format(getpass.getuser()))

        # Left column set signals
        main_window.ui.left_column.clicked.connect(main_window.btn_clicked)
        main_window.ui.left_column.released.connect(main_window.btn_released)

        # Set initial page / set left and right column menus
        MainFunctions.set_page(main_window, main_window.ui.load_pages.page_1)
        MainFunctions.set_left_column_menu(
            main_window,
            menu = main_window.ui.left_column.menus.menu_1,
            title = "Settings",
            icon_path = Functions.set_svg_icon("icon_settings.svg")
        )
        MainFunctions.set_right_column_menu(main_window, main_window.ui.right_column.menu_1)

        # Load settings
        settings = Settings()
        main_window.settings = settings.items

        # Load color theme
        themes = Themes()
        main_window.themes = themes.items


    def resize_grips(main_window):
        """
        Resize grips and change position
        Resize or change position when window is resized
        """
        if main_window.settings["custom_title_bar"]:
            main_window.left_grip.setGeometry(5, 10, 10, main_window.height())
            main_window.right_grip.setGeometry(main_window.width() - 15, 10, 10, main_window.height())
            main_window.top_grip.setGeometry(5, 5, main_window.width() - 10, 10)
            main_window.bottom_grip.setGeometry(5, main_window.height() - 15, main_window.width() - 10, 10)
            main_window.top_right_grip.setGeometry(main_window.width() - 20, 5, 15, 15)
            main_window.bottom_left_grip.setGeometry(5, main_window.height() - 20, 15, 15)
            main_window.bottom_right_grip.setGeometry(main_window.width() - 20, main_window.height() - 20, 15, 15)