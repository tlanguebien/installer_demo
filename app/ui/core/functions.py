#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from app.UserPreferences import UserPreferences

# Get user's preferences
user_preferences = UserPreferences()

# Images path
app_dir = os.getcwd()
images_path = os.path.join(app_dir, "app/ui/images")

class Functions:
    def set_svg_icon(icon_name):
        path = os.path.join(images_path, "svg_icons")
        icon = os.path.normpath(os.path.join(path, icon_name))
        return icon

    def set_svg_image(icon_name):
        """
        Sets SVG image
        """
        path = os.path.join(images_path, "svg_images")
        icon = os.path.normpath(os.path.join(path, icon_name))
        return icon

    def set_image(image_name):
        """
        Sets an image
        """
        path = os.path.join(images_path, "images")
        image = os.path.normpath(os.path.join(path, image_name))
        return image