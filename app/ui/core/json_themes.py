import json
import os
from app.ui.core.json_settings import Settings
from app.UserPreferences import UserPreferences

class Themes(object):
    # Load settings
    setup_settings = Settings()
    _settings = setup_settings.items

    # Get user's preferences
    user_preferences = UserPreferences()

    # App path
    themes_path = os.path.join(os.getcwd(), "app/ui/themes")
    selected_theme = _settings['default_theme']
    theme_json = "{}.json".format(selected_theme)
    settings_path = os.path.join(themes_path, theme_json)

    if not os.path.isfile(settings_path):
        print(f"WARNING: \"gui/themes/{_settings['theme_name']}.json\" not found! check in the folder {settings_path}")


    def __init__(self):
        super(Themes, self).__init__()
        # Gets the settings dictionary
        self.items = {}
        self.deserialize()


    def serialize(self):
        # Write json file
        with open(self.settings_path, "w", encoding='utf-8') as write:
            json.dump(self.items, write, indent=4)


    def deserialize(self):
        # Read json file
        with open(self.settings_path, "r", encoding='utf-8') as reader:
            settings = json.loads(reader.read())
            self.items = settings