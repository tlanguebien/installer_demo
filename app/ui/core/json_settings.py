#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import os
from app.UserPreferences import UserPreferences


class Settings():
    json_file = "settings.json"
    user_preferences = UserPreferences()
    app_dir = os.getcwd()

    settings_path = os.path.normpath(os.path.join(app_dir, "app", "ui", json_file))
    if not os.path.isfile(settings_path):
        print(f"WARNING: \"settings.json\" not found! check in the folder {settings_path}")
    

    def __init__(self):
        # Dictionary with settings, just to have objects references
        self.items = {}
        self.deserialize()


    def serialize(self):
        # Write json file
        with open(self.settings_path, "w", encoding='utf-8') as write:
            json.dump(self.items, write, indent=4)


    def deserialize(self):
        # Read json file
        with open(self.settings_path, "r", encoding='utf-8') as reader:
            settings = json.loads(reader.read())
            self.items = settings