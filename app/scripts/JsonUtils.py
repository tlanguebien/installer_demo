"""
    utilitary module for json

"""
import json
import os

def read_json(file_to_read):
    """
    read_json read a json

    :param file_to_read: path to json
    :type file_to_read: string
    :return: the json load
    :rtype: dict
    """
    if not check_json_exist(file_path=file_to_read):
        return
    with open(file_to_read, 'r') as f:
        return json.load(f)

def write_json(data,file_to_write):
    """
    write_json write a json

    :param data: data to write in json
    :type data: dict/obj
    :param file_to_write: path to json
    :type file_to_write: string
    """
    with open(file_to_write, 'w') as f:
        json.dump(data,f,indent=4)


def write_value(key, value, file):
    """
    edits a single value in the json
    """
    data = read_json(file)
    data[key] = value
    write_json(data, file)


def check_json_exist(file_path):
    return os.path.exists(file_path)
