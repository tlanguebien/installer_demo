#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import datetime
import pathlib

def add_log(log_string="", script_name="", separator=False):
    """
    This function adds a log
    """

    log_folder = os.getcwd()
    log_file = os.path.join(log_folder, "MyApp.log")
    log_date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    if script_name :
        script_name = "({})".format(script_name)

    #Create the log file if needed
    if not os.path.isfile(log_file) :
        try:
            os.mkdir(log_folder)
        except:
            pass
        f = open(log_file, "w", encoding='utf-16-le')
        f.write(u"Initializing log file...\n")
        f.close()

    #Add content
    if log_string :
        f = open(log_file, "a", encoding='utf-16-le')
        f.write(u"\n{} {} - {}\n".format(log_date, script_name, log_string))
        f.close()
        
    #Add separator
    if separator :
        f = open(log_file, "a", encoding='utf-16-le')
        f.write(u"\n------------------------------------------------------\n")
        f.close()
