import pymongo
from pymongo.errors import ConnectionFailure,OperationFailure
import socket

class Database():
    """
    This class's purpose is to ineract with the mongoDB databases.
    To access the MongoDB service, one needs the IP, port and credentials
    These are defined under "MongoDB Static Settings".

    After MongoDB has been initialized, one can access the databases.
    There is one Database per project.
    A Database contains Collections
    A Collection contains Documents
    Documents can contain any type of information (string, int, list...)
    Please note that a document can be created without creating a collection
    first : the collection is automatically created.

    The static methods can be used to query the service, and read/write stuff.
    """

    # Database Settings
    database = None # mongoDB object
    database_name = "app_db" # name (string)
    database_previous = None # Useful to jump to another DB and come back
    initialized = False

    @staticmethod
    def test_localhost_mongodb(port=27017):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            working = (s.connect_ex(('localhost', port)) == 0)
            return working

    @staticmethod
    def initialize():
        """
        Initializes the service with the static settings
        Example :
            Database.initialize()
        """
        if Database.initialized:
            return
        Database.initialized = True

        if not Database.test_localhost_mongodb():
            print("mongod.exe is not running")
            return
        
        Database.client = pymongo.MongoClient('localhost', 27017)
        failure = False
        try:
            Database.client.admin.command('ismaster')
        except (ConnectionFailure, OperationFailure):
            failure = True
            print("Server not available")
        if not failure:
            Database.refresh_db_list()
            if Database.database_name is None:
                pass
            else :
                Database.connect_db()


    @staticmethod
    def connect_db(name = None):
        """
        Connect to a database
        Example :
            Database.connect_db("My_Database")
        """
        if name is not None:
            Database.database_previous = Database.database_name
            Database.database_name = str(name)
        Database.database = Database.client[Database.database_name]


    @staticmethod
    def refresh_db_list():
        """
        Refresh the list of the available databases as a list of strings
        The list of dbs can then be accessed by calling Database.db_list
        Examples :
            Database.refresh_db_list
            print(Database.db_list)
        """
        Database.db_list = []
        for db in Database.client.list_databases():
            Database.db_list.append(db["name"])
        return Database.db_list
